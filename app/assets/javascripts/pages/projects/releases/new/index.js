import ZenMode from '~/zen_mode';
import initNewRelease from '~/releases/mount_new';

new ZenMode(); // eslint-disable-line no-new
initNewRelease();
